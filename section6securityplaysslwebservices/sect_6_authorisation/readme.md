# Cross site request forgery - Token method

    activator clean compile

    activator run

### AT POST MAN
     content type : text/html
     GET
     http://localhost:9000/authRequest
     go to headers
     key value

    SecurityArtifacts {

    String   CSRF_TOKEN = "this_shoudd_be_dynamic";
    global.java demo.token = "123";
    ...
    }
    NAME(key) : this_shoudd_be_dynamic
    value: 123
    
##  CSRF TOKEN
    
    https://en.wikipedia.org/wiki/Cross-site_request_forgery

### New data
    BY CURL:
    curl -i -H "this_shoudd_be_dynamic:123" -H "Content-Type: text/html" http://localhost:9000/authRequest

`HTTP/1.1 200 OK
Content-Type: text/plain; charset=utf-8
Date: Sun, 05 Feb 2017 16:50:21 GMT
Content-Length: 30
secured access for user:`

     BY HTTPIE
     http http://localhost:9000/html this_shoudd_be_dynamic:123

`HTTP/1.1 200 OK
Content-Length: 30
Content-Type: text/plain; charset=utf-8
Date: Sun, 05 Feb 2017 16:52:42 GMT
secured access for user:  demo`


    http http://localhost:9000/authRequest Content-Type:text/html this_shoudd_be_dynamic:123
    http https://sect6-authorization1.herokuapp.com/authRequest Content-Type:text/html this_shoudd_be_dynamic:123

`HTTP/1.1 200 OK
Connection: close
Content-Type: text/plain; charset=utf-8
Date: Sun, 05 Feb 2017 16:54:10 GMT
Server: Cowboy
Via: 1.1 vegur
secured access for user:  demo`

### Deployed to heroku

    https://sect6-authorization1.herokuapp.com/ 