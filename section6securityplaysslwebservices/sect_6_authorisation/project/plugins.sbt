// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.4.1")
// The Ebean bytecode enhancement functionality has also been extracted out of the Play sbt plugin into its own plugin
addSbtPlugin("com.typesafe.sbt" % "sbt-play-ebean" % "1.0.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.1.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-mocha" % "1.1.0")
