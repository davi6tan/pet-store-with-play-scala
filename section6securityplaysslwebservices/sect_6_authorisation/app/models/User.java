package models;

import play.db.ebean.Model;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User extends Model {


    @Id
    public Integer id;
    public String name;
    public String token;
    public static Finder<Integer, User> find = new Finder<Integer,User>(User.class);
}
