package controllers;

import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import models.User;

public class ProtectedResource extends Action.Simple  implements SecurityArtifacts {


    private String procesTokenHeader(Http.Context context) {
        String[] token = context.request().headers().get(CSRF_TOKEN);
        if ((token != null) && (token.length == 1) && (token[0] != null)) {
            return token[0];
        }
        return null;
    }


    public F.Promise<Result> call(Http.Context context) throws Throwable {
        String token = procesTokenHeader(context);
        if (token != null) {
            User user = User.find.where().eq(CSRF_TOKEN_KEY, token).findUnique();
            if (user != null) {
                context.request().withUsername(user.name);
                return delegate.call(context);
            }
        }
        Result unauthorized = Results.unauthorized(UNAUTHORIZED);
        return F.Promise.pure(unauthorized);
    }
}