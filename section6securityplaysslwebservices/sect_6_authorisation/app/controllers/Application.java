package controllers;

import play.mvc.*;

public class Application extends Controller {

    public static Result unauthenticatedAction() {
        return ok("Unsecured resource" );
    }

    @Security.Authenticated(RouteAuthenticator.class)
    public static Result authenticatedRoute() {

        return ok("secured access for user:  " + request().username());
    }

    @With(ProtectedResource.class)
    public static Result securedAction() {

        return ok("secured access for user:  " + request().username());
    }

}
