package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

@With(ProtectedResource.class)
public class ProtectedResourceController extends Controller {

    public static Result authenticatedRoute() {

        return ok("secured access for a Protected Resource Controller ") ;
    }
}
