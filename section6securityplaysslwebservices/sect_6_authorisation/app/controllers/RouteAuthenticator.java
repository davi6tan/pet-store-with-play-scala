package controllers;

import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import models.User;

public class RouteAuthenticator extends Security.Authenticator implements SecurityArtifacts {


    @Override
    public String getUsername(Http.Context ctx) {
        String token = procesTokenHeader(ctx);
        if (token != null) {
            User user = User.find.where().eq(CSRF_TOKEN_KEY, token).findUnique();
            if (user != null) {
                return user.name;
            }
        }
        return null;
    }

    @Override
    public Result onUnauthorized(Http.Context context) {
        return super.onUnauthorized(context);
    }


    private String procesTokenHeader(Http.Context ctx) {
        String[] authTokenHeaderValues = ctx.request().headers().get(CSRF_TOKEN);
        if ((authTokenHeaderValues != null) &&
                (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
            return authTokenHeaderValues[0];
        }
        return null;
    }
}