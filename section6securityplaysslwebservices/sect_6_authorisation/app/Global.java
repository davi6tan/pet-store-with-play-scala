import models.User;
import play.Application;
import play.GlobalSettings;

/**
 * Created by ubu on 11.05.16.
 */
public class Global extends GlobalSettings {


    /**
     * Sync the context lifecycle with Play's.
     */
    @Override
    public void onStart(final Application app) {
        super.onStart(app);

        User demo = new User();
        demo.name = "demo";
        demo.token = "123";
        demo.save();

    }

}
