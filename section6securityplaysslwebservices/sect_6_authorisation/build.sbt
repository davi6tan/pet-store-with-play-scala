name := """sect_6_auth"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean )

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "com.typesafe.play" %% "play-slick" % "0.9.0-M1",
  "org.mindrot" % "jbcrypt" % "0.3m"
)
