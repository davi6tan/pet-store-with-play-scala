name:= "spark"

//Spark dependencies
val sparkVersion = "1.4.1"
libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-streaming" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided"
)

//Spark Cassandra connector
libraryDependencies += "com.datastax.spark" %% "spark-cassandra-connector" % "1.4.0-M3"

// Spark Kafka
libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka" % "1.4.1"

