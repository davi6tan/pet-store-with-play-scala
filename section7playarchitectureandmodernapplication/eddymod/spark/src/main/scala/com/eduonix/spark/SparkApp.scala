package com.eduonix.spark

import com.eduonix.common.model.CoreJsonFormats
import com.eduonix.common.kafka.utils.KafkaConfig
import CoreJsonFormats._
import com.eduonix.common.model.{DataEntry }
import com.eduonix.common.util.ConfigLoader
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.{SQLContext, SaveMode}
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import play.api.libs.json.Json

object SparkApp extends App {
  dataEntryFormat

  @transient lazy val log =
    Logger.getLogger(getClass.getName)

  val conf = ConfigLoader.conf

  def createContextFunc(): StreamingContext = {
    log.info("#### CREATE NEW SPARK STREAMING CONTEXT ####")

    val cassandraHost = conf.getString("cassandra.host")

    val sparkConf = new SparkConf()/*.setMaster(masterPath)*/.setAppName("SparkContext")
      .set("spark.cassandra.connection.host", cassandraHost)

    val ssc = new StreamingContext(sparkConf, Seconds(1))
    ssc
  }

  // Get StreamingContext from checkpoint data or create a new one
  val ssc = createContextFunc()

  val sc = ssc.sparkContext
  val sqlContext = new SQLContext(sc)

  import sqlContext.implicits._

  val zkQuorum = KafkaConfig().getCustomString(KafkaConfig.zookeeperConnect)
  val groupId = KafkaConfig().getCustomString(KafkaConfig.groupId)
  val kafkaParams = Map[String, String]("metadata.broker.list" -> KafkaConfig().getCustomString(KafkaConfig.brokers))

  private val dStream =
    KafkaUtils.createStream(
      ssc = ssc,
      zkQuorum = zkQuorum,
      groupId = groupId,
      topics = Map[String,Int]("data" -> 1)
    )
      .flatMap(t => Json.parse(t._2).as[List[DataEntry]])



  ssc.start()
  ssc.awaitTermination()

}
