import com.github.simplyscala.{MongoEmbedDatabase, MongodProps}
import com.mongodb.casbah.WriteConcern
import controllers.api.Users
import models.User
import org.scalatest.{GivenWhenThen, _}
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

class ApplicationSpec extends FunSpec with GivenWhenThen with Matchers
                            with MongoEmbedDatabase with BeforeAndAfterAll {

  var mongoProps: MongodProps = null

  override protected def beforeAll(): Unit = mongoProps = mongoStart(port = 27018)

  override protected def afterAll(): Unit = mongoStop(mongoProps)

  describe("Application Configuration Test") {
    val firstName = "eddy"
    val lastName = "mod"
    val email = "eddy@eddymod.com"
    val password = "eddymod123"
    var token = ""
    describe("POST /user") {
      it("should create a user") {
        running(FakeApplication()) {
          Given("no parameter")

          val json = Json.toJson(
            Map(
              "firstName" -> firstName,
              "lastName" -> lastName,
              "email" -> email,
              "password" -> password
            )
          )
          val result = route(FakeRequest(POST, "/user", FakeHeaders(), json)).get

          Then("StatusCode is 200")
          status(result) shouldBe OK

          And("ContentType is application/json")
          contentType(result).get shouldBe "application/json"
        }
      }
    }


  }

}