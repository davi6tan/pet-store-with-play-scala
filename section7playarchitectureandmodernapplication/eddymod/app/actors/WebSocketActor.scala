package actors

import java.util.Date

import akka.actor.ActorRef
import models.User
import akka.actor._


class WebSocketActor(socketSession: SocketSession) extends Actor {
  def receive = {
    case msg: String =>
      socketSession.out ! ("I received your message: " + msg)
  }

  override def postStop() = {

  }
}

object WebSocketActor {

  def props(socketSession: SocketSession) = {
    Props(new WebSocketActor(socketSession))
  }
}

case class SocketSession(uuid: String, user: User, out: ActorRef)

case class PingMessage(date: String = new Date().toString)

case class StopMessage(uuid: String)