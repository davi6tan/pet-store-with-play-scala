package util

object Logger {

  def apply(name: String): play.api.Logger = {
    play.api.Logger(s"cardio.$name")
  }

  def apply[T](clazz: Class[T]): play.api.Logger = {
    var name = clazz.getName
    if (name.endsWith("$")) name = name.substring(0, name.length - 1)
    apply(name)
  }

}
