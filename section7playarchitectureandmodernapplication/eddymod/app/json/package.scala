import play.api.libs.json.{Json, Reads}

package object json {
  def parseTo[T](s:String)(implicit fjs: Reads[T]): T = Json.parse(s).as[T]

}
