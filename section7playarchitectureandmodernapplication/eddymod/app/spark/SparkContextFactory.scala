package spark

import com.eduonix.common.util.ConfigLoader
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.cassandra.CassandraSQLContext
import org.apache.spark.{SparkConf, SparkContext}


object SparkContextFactory {
  val sc = new SparkContext(createSparkConf)
  val sQLContext = new SQLContext(sc)

  val cassandraSQLContext = new CassandraSQLContext(sc)

  def startSparkContext(): Unit = {}

  def stopSparkContext() = sc.stop()

  private def createSparkConf: SparkConf = {
    val conf = ConfigLoader.conf

    val masterPath = conf.getString("spark.master.path")
    val cassandraHost = conf.getString("cassandra.host")

    new SparkConf().setMaster(masterPath).setAppName("SparkContext")
      .set("spark.cassandra.connection.host", cassandraHost)
  }
}
