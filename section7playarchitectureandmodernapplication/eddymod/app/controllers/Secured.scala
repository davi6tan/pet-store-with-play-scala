package controllers

import controllers.api.Users
import models.User
import play.api.mvc._

import scala.concurrent.Future

trait Secured {

  def token(request: RequestHeader) = request.headers.get(Users.AUTH_TOKEN)

  def onUnauthorized(request: RequestHeader) = Results.Unauthorized
  def asyncOnUnauthorized(request: RequestHeader) = Future.successful(Results.Unauthorized)

  def withAuth[A](bp:BodyParser[A])(f: => String => Request[A] => Result) = {
    Security.Authenticated(token, onUnauthorized) { token =>
      Action(bp)(request => f(token)(request))
    }
  }
  def withAuth(f: => String => Request[AnyContent] => Result) = {
    Security.Authenticated(token, onUnauthorized) { token =>
      Action(request => f(token)(request))
    }
  }

  def asyncWithAuth(f: => String => Request[AnyContent] => Future[Result]) = {
    Security.Authenticated(token, onUnauthorized) { token =>
      Action.async(request => f(token)(request))
    }
  }

  def asyncWithUser(f: User => Request[AnyContent] => Future[Result]) = asyncWithAuth { token => implicit request =>
    User.findOneByAuthToken(token).map { user =>
      f(user)(request)
    }.getOrElse(asyncOnUnauthorized(request))
  }

  def withUser(f: User => Request[AnyContent] => Result) = withAuth { token => implicit request =>
    User.findOneByAuthToken(token).map { user =>
      f(user)(request)
    }.getOrElse(onUnauthorized(request))
  }

  def withUser[A](bp: BodyParser[A])(f: User => Request[A] => Result) = withAuth(bp) { token => implicit request =>
    User.findOneByAuthToken(token).map { user =>
      f(user)(request)
    }.getOrElse(onUnauthorized(request))
  }
}
