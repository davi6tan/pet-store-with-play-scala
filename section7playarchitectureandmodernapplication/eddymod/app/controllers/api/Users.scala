package controllers.api

import com.mongodb.casbah.WriteConcern
import controllers.Secured
import models.JsonFormats._
import models.{LoginRequest, User, UserRequest}
import play.api.libs.json._
import play.api.mvc._

object Users extends Controller with Secured {
  val AUTH_TOKEN: String = "authToken"

  def create = Action(parse.json) {
    request =>
      request.body.validate[UserRequest].asOpt match {
        case Some(userRequest) =>
          // Validation
          val validationResults = userRequest.validate()
          if (validationResults.nonEmpty) BadRequest(Json.toJson(validationResults))
          else {
            val user = userRequest.toUser
            User.save(user, WriteConcern.Safe)
            User.findOneById(user.id) match {
              case Some(u) => Ok(Json.toJson("{}"))
              case None => InternalServerError("failed to create user")
            }
          }
        case None => BadRequest("Can't parse to UserRequest")
      }
  }

  def login = Action(parse.json) { implicit request =>
    request.body.validate[LoginRequest].asOpt match {
      case Some(loginRequest) =>

      val usr =   User.authenticate(loginRequest.email, loginRequest.password)

        usr match {
          case Some(user) =>
            val token = User.createToken(user)
            Ok(Json.toJson(Map(AUTH_TOKEN -> token)))
          case None => Unauthorized
        }
      case None => BadRequest
    }
  }

  def get = withUser { user => implicit request =>
    Ok(Json.toJson(user))
  }

  def logout = withUser { user => implicit request =>
    val token = request.headers(Users.AUTH_TOKEN)
    User.save(User.deleteAuthToken(user, token),WriteConcern.Safe)
    Ok("{}")
  }

  def update = withUser(parse.json) { user => implicit request =>
    request.body.validate[UserRequest].asOpt match {
      case Some(userRequest) =>

        val updatedUser = user.copy(
          email = userRequest.email,
          password = User.getSha512(userRequest.password),
          firstName = userRequest.firstName,
          lastName = userRequest.lastName
        )
        User.save(updatedUser, WriteConcern.Safe)
        Ok(Json.toJson(updatedUser))

      case None => BadRequest
    }
  }

  def delete = withUser { user => implicit request =>
    User.removeById(user.id)
    Ok
  }
}


