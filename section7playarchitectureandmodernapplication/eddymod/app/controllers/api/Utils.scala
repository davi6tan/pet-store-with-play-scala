package controllers.api

import play.api.libs.iteratee._
import play.api.mvc._
import util.Logger
import scala.concurrent.ExecutionContext.Implicits.global

object Utils extends Controller {
  val logger = Logger(this.getClass)

  // load balancer health check
  def ping = Action {
    Ok("pong")
  }

  // ws echo tool
  def echo = WebSocket.using[String] {
    request => {
      // create PushEnumerator
      val (out, channel) = Concurrent.broadcast[String]
      val in = Iteratee.foreach[String](text => {
        logger.info(text)
        // push
        channel.push(text)
      }).map(_ => {
        // on connection disconnected
        logger.info("Disconnected")
      })
      in -> out
    }
  }
}
