package controllers.api


import controllers.Secured
import models.DataRecord
import play.api.mvc.Controller
import util.Logger

object DataProcess extends Controller with Secured {
  private val logger = Logger(this.getClass)

  def deleteAll() = withUser { user => implicit request =>
    DataRecord.findAllByUserId(user.id).foreach(_.delete())
    Ok
  }





}

