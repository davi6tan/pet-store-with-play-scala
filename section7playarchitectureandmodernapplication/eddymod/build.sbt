import play.PlayImport.PlayKeys._

name := "eddymod"

version := "1.0"

lazy val playSettings = Seq(
  scalaVersion := "2.11.5"
)

lazy val sparkSettings = Seq(
  scalaVersion := "2.11.5"
)
routesImport += "se.radley.plugin.salat.Binders._"
TwirlKeys.templateImports += "org.bson.types.ObjectId"

lazy val eddymod = (project in file(".")).settings(playSettings: _*).enablePlugins(PlayScala)
  .dependsOn(common)
  .aggregate(common)

lazy val common = project.settings(

  scalaVersion := "2.11.5"
)

lazy val spark = project.settings(sparkSettings: _*).dependsOn(common)


//Play dependencies
libraryDependencies ++= Seq(jdbc, cache, ws, filters)

//Spark dependencies
val sparkVersion = "1.4.1"
libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion
)

//Spark Cassandra connector
libraryDependencies += "com.datastax.spark" %% "spark-cassandra-connector" % "1.4.0-M3"

// Spark Kafka
libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka" % "1.4.1"

// Spark csv
libraryDependencies += "com.databricks" %% "spark-csv" % "1.2.0"


// Reactive Kafka
libraryDependencies += "com.softwaremill.reactivekafka" %% "reactive-kafka-core" % "0.8.0"

//Salat dependencies
libraryDependencies += "net.cloudinsights" %% "play-plugins-salat" % "1.5.9"



// Test dependencies
libraryDependencies ++= Seq(
  "org.scalatestplus" %% "play" % "1.2.0",
  "com.github.simplyscala" %% "scalatest-embedmongo" % "0.2.2" % "test"
)

resolvers += "sbt-plugin-snapshots" at "http://repo.scala-sbt.org/scalasbt/sbt-plugin-releases/"

resolvers += Resolver.sonatypeRepo("snapshots")


ivyScala := ivyScala.value map {
  _.copy(overrideScalaVersion = true)
}
