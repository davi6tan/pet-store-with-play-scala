name:= "common"


version:= "0.1-SNAPSHOT"

libraryDependencies += "org.apache.kafka" %% "kafka" % "0.8.2.1"

// Play json
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.3.9"

libraryDependencies += "org.scalanlp" %% "breeze" % "0.11.2"


// Test dependencies
libraryDependencies ++= Seq(
  "org.scalatestplus" %% "play" % "1.2.0" % "test"
)

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
