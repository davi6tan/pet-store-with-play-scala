package com.eduonix.common.model

case class DataEntry(
                       id: String,
                       userid: String,
                       time: Long,
                       temp: String
                       )

case class DataEntries(data: List[DataEntry])