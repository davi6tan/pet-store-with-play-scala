package com.eduonix.common.model

import play.api.libs.json.Json

trait CoreJsonFormats {

  implicit val dataEntryFormat = Json.format[DataEntry]
  implicit val dataEntriesFormat = Json.format[DataEntries]

}

object CoreJsonFormats extends CoreJsonFormats