import com.mongodb.casbah.WriteConcern
import kafka.KafkaSystem
import models.User
import play.api.mvc.WithFilters
import play.api.{Application, GlobalSettings}
import play.filters.gzip.GzipFilter
import spark.SparkContextFactory

object Global extends WithFilters(new GzipFilter) with GlobalSettings {
  /** Starts SparkContext and Kafka */
  override def onStart(application: Application): Unit = {
    SparkContextFactory.startSparkContext()
   // KafkaSystem.start()

  }
  /** Stops SparkContext */
  override def onStop(application: Application): Unit = {
    SparkContextFactory.stopSparkContext()
  }
}