package controllers

import actors.{EchoWebSocketActor}
import play.libs._
import play.api.mvc._

object WebSocketApi  extends Controller {


  //  tests configuration
  def ping = Action {
    Ok("pong")
  }


  import play.api.Play.current
  def echo = WebSocket.acceptWithActor[String, String] {
    request =>
      out => {
        println("wsWithActor, client connected")
        EchoWebSocketActor.props(out)
      }
  }



}
