package spark

import org.apache.spark.sql.DataFrame


/** DataLoader object */
object CassandraDAO {

  SparkContextFactory.cassandraSQLContext.setKeyspace("eddy")

  def loadFromCassandra(tableName: String ) : DataFrame = {
    SparkContextFactory.cassandraSQLContext.sql(s"select * from $tableName")
  }

  def insertrecorfIntoCassandra(tableName: String, values: String, dataRecord: String ) : DataFrame = {
    println(s"INSERT INTO  $tableName $values VALUES  $dataRecord")
    SparkContextFactory.cassandraSQLContext.sql(s"INSERT INTO  $tableName $values VALUES  $dataRecord")
  }



}
