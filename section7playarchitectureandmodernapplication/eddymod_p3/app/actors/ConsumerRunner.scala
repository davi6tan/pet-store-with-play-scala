package actors


import com.eduonix.common.kafka.consumer.KConsumer

/**
  * Created by ubu on 18.05.16.
  */
object ConsumerRunner extends App {

  val example = new KConsumer("eddydata" )
  example.run( )

}
