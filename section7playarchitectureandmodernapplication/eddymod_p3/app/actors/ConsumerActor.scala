package actors

import akka.actor.{Actor, ActorSystem, Props}
import akka.stream.ActorMaterializer
import com.eduonix.common.kafka.consumer.KConsumer
import util.Logger


/**
  * Created by ubu on 19.05.16.
  */
class ConsumerActor  extends Actor  {

  private val logger = Logger(this.getClass)

  private implicit val materializer = ActorMaterializer()


  // This is the behavior when it's active
  def inactive: Receive = {

    case  "start"  =>
      context.become(active)
      self ! "next"

  }


  // This is the behavior when it's active
  def active: Receive = {
    case "stop" =>
      logger.info(s"ConsumerActor received STOP message ")
      context.become(inactive)
    case "next" =>
      consumeResults()

    case "kill" =>
      context.stop(self)
  }

  def receive = inactive // Start out as inactive


  def consumeResults() = {


    val example = new KConsumer("eddydata" )
    example.run( )

  }

}

object ConsumerActor {
  lazy val system = ActorSystem("consumerSystem")

  def newInstance = {
    system.actorOf(Props(new ConsumerActor))
  }
}