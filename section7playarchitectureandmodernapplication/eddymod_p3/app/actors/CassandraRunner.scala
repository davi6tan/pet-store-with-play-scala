package actors

import spark.CassandraDAO

import scala.collection.mutable.ListBuffer

/**
  * Created by ubu on 21.05.16.
  */
object CassandraRunner extends App {

  val VALUES_STRING  = """(id, data)"""
  val TABLE_STRING =  """data"""

  CassandraDAO.loadFromCassandra(TABLE_STRING).show()


}
