package models

import com.mongodb.casbah.{MongoClient, MongoDB}
import com.eduonix.common.util.ConfigLoader

object Mongo {
  def apply(): MongoDB = {
    val dbName = ConfigLoader.conf.getString("mongodb.default.db")
    val host = ConfigLoader.conf.getString("mongodb.default.host")
    val port = ConfigLoader.conf.getInt("mongodb.default.port")

    println(s"debug !!!!!!!!!!   $dbName $host $port   !!!!!!!!!!!!!!!!")

    MongoClient(host,port)(dbName)
  }
}
