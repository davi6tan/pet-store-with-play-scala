package models

import java.util.Date

import com.mongodb.casbah.Imports._
import com.novus.salat.dao.{ModelCompanion, SalatDAO}
import org.bson.types.ObjectId

case class DataRecord(id: ObjectId = new ObjectId(),
                      userId: ObjectId, name: String,
                      min: Long = -1, max: Long = -1,
                      samplingFrequency: Double,
                      creationDate: Date = new Date
                     ) {

  object path extends Serializable {

    val recordDir = s"/"
    val csv = s"$recordDir/csv"

  }

  def checkRange(min: Long, max: Long) = {
    if (this.min <= min && this.max >= max) true
    else false
  }

  def delete(): Unit = {
    DataRecord.remove(this, WriteConcern.Safe)
  }
}

object DataRecord extends DataRecordDAO

trait DataRecordDAO extends ModelCompanion[DataRecord, ObjectId] {
  def collection = Mongo()("datarecords")

  val dao = new SalatDAO[DataRecord, ObjectId](collection) {}

  // Queries
  def findAllByUserId(userId: ObjectId): List[DataRecord] = dao.find(MongoDBObject("userId" -> userId)).toList

  def findOneByIdAndUserId(id: ObjectId, userId: ObjectId) = dao.findOneById(id) match {
    case Some(b) => if (b.userId == userId) Some(b) else None
    case None => None
  }

}



