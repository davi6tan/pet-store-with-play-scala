package models

import com.eduonix.common.model.CoreJsonFormats
import org.bson.types.ObjectId
import play.api.libs.json._

object JsonFormats extends CoreJsonFormats {

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {

    def reads(json: JsValue) = {
      json match {
        case jsString: JsString =>
          if (ObjectId.isValid(jsString.value)) JsSuccess(new ObjectId(jsString.value))
          else JsError("Invalid ObjectId")
        case other => JsError("Can't parse json path as an ObjectId. Json content = " + other.toString())
      }
    }

    def writes(oId: ObjectId): JsValue = JsString(oId.toString)
  }

  implicit val userRequestReads = Json.reads[UserRequest]

  implicit val loginRequestReads = Json.reads[LoginRequest]

  implicit val validationResultWrites = Json.writes[ValidationResult]



  import actors.WebSocketActor._
  implicit val eventFormat = Json.format[EchoMessage]

  import play.api.mvc.WebSocket.FrameFormatter
  implicit val eventFormatFrameFormatter = FrameFormatter.jsonFrame[EchoMessage]





  implicit object UserFormat extends Format[User] {

    def reads(json: JsValue): JsResult[User] = {
      val user = User(
        email = (json \ "email").as[String],
        password = User.getSha512((json \ "password").as[String]),
        firstName = (json \ "firstName").as[String],
        lastName = (json \ "lastName").as[String]
      )
      JsSuccess(user)
    }

    def writes(o: User): JsValue = Json.obj(
      "id" -> Json.toJson(o.id),
      "email" -> Json.toJson(o.email),
      "firstName" -> Json.toJson(o.firstName),
      "lastName" -> Json.toJson(o.lastName)
    )
  }



}
