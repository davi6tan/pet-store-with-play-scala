package kafka

import java.util.{Properties, UUID}

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.eduonix.common.kafka.utils.KafkaConfig
import com.eduonix.common.util.ConfigLoader
import com.softwaremill.react.kafka.KafkaMessages._
import com.softwaremill.react.kafka.{ConsumerProperties, ReactiveKafka}
import kafka.serializer.StringDecoder
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkEnv}
import org.reactivestreams.Publisher
import util.Logger

import scala.collection.mutable.ListBuffer


object KafkaSystem {
  private val logger = Logger(this.getClass)
  logger.info("start reactive kafka")

  val kafka_topic = "eddydata"

  private val kafka = new ReactiveKafka()
  private implicit val system = ActorSystem("KafkaSystem")
  private implicit val materializer = ActorMaterializer()
  private val zkHost = KafkaConfig()
    .getCustomString(KafkaConfig.zookeeperConnect)
  private val brokerList = KafkaConfig().getCustomString(KafkaConfig.brokers)

  private val rstreamConsumer: Publisher[StringKafkaMessage] = createConsumer(kafka_topic)



  def writeMessage(): Unit = {
    val producer :KafkaProducer[String, String] = createKafkaProducer( )

    val message =
      new ProducerRecord[String, String](kafka_topic, null, "this is a reactive message")
    producer.send(message)

  }


  def createKafkaProducer( ): KafkaProducer[String, String] = {

    lazy val kafkaProducerParams = new Properties()
    kafkaProducerParams.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerList)
    kafkaProducerParams
      .put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    kafkaProducerParams
      .put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    val producer = new KafkaProducer[String, String](kafkaProducerParams)
    producer
  }


  def createConsumer(topic: String): Publisher[StringKafkaMessage] = {

    kafka.consume(ConsumerProperties(
      brokerList = brokerList,
      zooKeeperHost = zkHost,
      topic = kafka_topic,
      groupId = s"ReactiveKafka-${UUID.randomUUID().toString}",
      decoder = new StringDecoder()
    ).readFromEndOfStream())

  }





  def createStreamingContext(): StreamingContext = {

    val conf = ConfigLoader.conf
    val cassandraHost = conf.getString("cassandra.host")
    val sparkConf = new SparkConf() /*.setMaster(masterPath)*/ .setAppName("SparkContext")
      .set("spark.cassandra.connection.host", cassandraHost).setMaster("local[*]")
    val ssc = new StreamingContext(sparkConf, Seconds(1))
    ssc
  }



}
