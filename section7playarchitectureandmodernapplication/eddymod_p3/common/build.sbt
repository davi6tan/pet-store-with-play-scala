name:= "common"


version:= "0.1-SNAPSHOT"


libraryDependencies += "org.apache.kafka" %% "kafka" % "0.8.2.0"

// Play json
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.3.9"

libraryDependencies += "org.scalanlp" %% "breeze" % "0.11.2"

// http://mvnrepository.com/artifact/org.eclipse.jetty.websocket/websocket-client
libraryDependencies += "org.eclipse.jetty.websocket" % "websocket-client" % "9.3.9.M1"



// Test dependencies
libraryDependencies ++= Seq(
  "org.scalatestplus" %% "play" % "1.2.0" % "test"
)

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
