package com.eduonix.spark

import com.eduonix.common.kafka.producer.Producer
import com.eduonix.common.kafka.utils.KafkaConfig
import com.eduonix.common.model.CoreJsonFormats
import com.eduonix.common.model.CoreJsonFormats._
import com.eduonix.common.util.ConfigLoader
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import play.api.libs.json.Json

import scala.collection.mutable.ListBuffer

object ScalaDataGenerator {

  def runSparkDataProcess(): Unit = {
    dataEntryFormat

    @transient lazy val log =
      Logger.getLogger(getClass.getName)

    val conf = ConfigLoader.conf
    val cassandraHost = conf.getString("cassandra.host")
    val sparkConf = new SparkConf() /*.setMaster(masterPath)*/ .setAppName("SparkContext")
      .set("spark.cassandra.connection.host", cassandraHost).setMaster("local[*]")
    val ssc = new StreamingContext(sparkConf, Seconds(1))



    val sc = ssc.sparkContext
    val sqlContext = new SQLContext(sc)
    val recordsPath: String = ParseCsv.findResource("/ComercialBanks10k.csv")
    val records: Seq[CsvRecord] = ParseCsv.readFromCsvFile(recordsPath)(CsvRecord.parse)
    val tokens: Seq[CsvToken] = ParseCsv.readFromCsvRecords(records)(CsvToken.parse)
    val processedTokens: Seq[Seq[String]] = tokens.map(t => CsvToken.getProcessTokens(t.tokens)).drop(1)
    val nonEmptyTokens = ParseCsv.mapTonNnEmptyResults(processedTokens)
    println("testing")
    println(nonEmptyTokens.head)

      val zkQuorum = KafkaConfig().getCustomString(KafkaConfig.zookeeperConnect)
      val groupId = KafkaConfig().getCustomString(KafkaConfig.groupId)
      val kafkaParams = Map[String, String]("metadata.broker.list" -> KafkaConfig().getCustomString(KafkaConfig.brokers))

    // here we would write the data to cassandra
      val dStream =
        KafkaUtils.createStream(
          ssc = ssc,
          zkQuorum = zkQuorum,
          groupId = groupId,
          topics = Map[String,Int]("data" -> 1)
        )
          .flatMap(t => Json.parse(t._2).as[List[ListBuffer[String]]])



  // topic is eddydata
    val dataProducer = Producer[String]("eddydata")

    // here we would write the data to kafka
    dataProducer.send(Json.toJson(nonEmptyTokens).toString())


    sc.stop

//      ssc.start()
//      ssc.awaitTermination()

  }

}
