package com.eduonix.common.util

import com.typesafe.config.ConfigFactory

object ConfigLoader {
  val conf = load

  def load = {
    ConfigFactory.load()
  }
  def load(resourceName:String) = {
    ConfigFactory.load(resourceName)
  }

}
