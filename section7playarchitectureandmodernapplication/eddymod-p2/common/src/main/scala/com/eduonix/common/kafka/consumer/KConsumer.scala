package com.eduonix.common.kafka.consumer


import java.util.Properties
import java.util.concurrent._

import kafka.consumer.{Consumer, ConsumerConfig, KafkaStream}
import kafka.utils.Logging

/**
  * Created by ubu on 18.05.16.
  */
class KConsumer[A](topic: String) {

  def createConsumerConfig : ConsumerConfig = {
  val props = new Properties();
  props.put("group.id", "test");
  props.put("zookeeper.connect", "localhost:2181");
  props.put("auto.offset.reset", "largest");
  props.put("zookeeper.session.timeout.ms", "400");
  props.put("zookeeper.sync.time.ms", "200");
  props.put("auto.commit.interval.ms", "1000");
  val config = new ConsumerConfig(props)
    config
  }
  val consumer = Consumer.create(createConsumerConfig)
  var executor: ExecutorService = null

  def shutdown() = {
    if (consumer != null)
      consumer.shutdown();
    if (executor != null)
      executor.shutdown();
  }

  def run( ) = {
    val topicCountMap = Map(topic -> 3)
    val consumerMap = consumer.createMessageStreams(topicCountMap);
    val streams = consumerMap.get(topic).get;

    executor = Executors.newFixedThreadPool(3);
    var threadNumber = 0;
    for (stream <- streams) {
      executor.submit(new MessageResolver(stream, threadNumber, 10))
      threadNumber += 1
    }
  }



}

class MessageResolver(val stream: KafkaStream[Array[Byte], Array[Byte]], val threadNumber: Int, val delay: Long) extends Logging with Runnable {
  def run {
    val it = stream.iterator()

    while (it.hasNext()) {
      val msg = new String(it.next().message());
     println(System.currentTimeMillis()
       + ",Thread " + threadNumber + ": " + msg)
    }

    println("Shutting down Thread: " + threadNumber)
  }
}
