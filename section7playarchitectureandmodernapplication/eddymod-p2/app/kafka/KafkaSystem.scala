package kafka

import java.util.UUID
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.eduonix.common.kafka.utils.KafkaConfig
import com.eduonix.common.model.DataEntries
import com.softwaremill.react.kafka.KafkaMessages._
import com.softwaremill.react.kafka.{ConsumerProperties, ReactiveKafka}
import json._
import kafka.serializer.StringDecoder
import models.JsonFormats._
import org.apache.spark.SparkEnv
import org.reactivestreams.Publisher
import util.Logger


object KafkaSystem {
  private val logger = Logger(this.getClass)

  logger.info("start reactive kafka")
  private val kafka = new ReactiveKafka()
  private implicit val actorSystem = SparkEnv.get.actorSystem
  private implicit val materializer = ActorMaterializer()
  private val zkHost = KafkaConfig().getCustomString(KafkaConfig.zookeeperConnect)
  private val brokerList = KafkaConfig().getCustomString(KafkaConfig.brokers)
  private val hrvPublisher: Publisher[StringKafkaMessage] = createPublisher("hrv")
  private val rtDataPublisher: Publisher[StringKafkaMessage] = createPublisher("rtSocketData")


  logger.info("start rtDataPublisher")


  def start() = {}

  def createPublisher(topic: String): Publisher[StringKafkaMessage] = {

    kafka.consume(ConsumerProperties(
      brokerList = brokerList,
      zooKeeperHost = zkHost,
      topic = topic,
      groupId = s"ReactiveKafka-${UUID.randomUUID().toString}",
      decoder = new StringDecoder()
    ).readFromEndOfStream())

  }
}
