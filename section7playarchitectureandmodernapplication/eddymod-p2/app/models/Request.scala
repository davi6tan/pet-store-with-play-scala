package models


import com.eduonix.common.model.DataEntry
import org.bson.types.ObjectId
import play.i18n.Messages

import scala.collection.mutable

trait Request

case class UserRequest(
                        email: String,
                        password: String,
                        firstName: String,
                        lastName: String
                      ) {

  private val EmailRegex = "^([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$"
  private val PasswordRegex = "^.{6,18}$"

  def validate(): List[ValidationResult] = {
    val result = mutable.MutableList[ValidationResult]()

    if (!email.matches(EmailRegex)) result += ValidationResult("email", Messages.get("user.email"))
    if (User.findOneByEmail(email).nonEmpty) result += ValidationResult("email", Messages.get("user.email.unique"))
    if (!password.matches(PasswordRegex)) result += ValidationResult("password", Messages.get("user.password"))
    result.toList
  }

  def toUser: User = {
    User(
      email = email,
      password = User.getSha512(password),
      firstName = firstName,
      lastName = lastName
    )
  }
}

case class ValidationResult(field: String, message: String)

case class LoginRequest(email: String, password: String)

case class EntryRequest(
                         time: Long,
                         temp: String
                       ) {
  def toDataEntry(userId: ObjectId) =
    DataEntry(
      id = Id(time, userId.toString),
      userid = userId.toString,
      time = time,
      temp = temp
    )
}

case class RealTimeDataRequest(id: Long, data: List[EntryRequest]) {
  def toDataEntryList(userId: ObjectId) = {
    data.map(_.toDataEntry(userId))
  }
}

case class RealTimeDataResponse(id: Long, data: List[DataEntry])

object EntryRequest {
  def fromDataEntry(d: DataEntry) = EntryRequest(
    time = d.time,
    temp = d.temp
  )
}





case class EntryRequestListContainer(data: List[EntryRequest])

object Id {
  def apply(time: Long, userId: String) = {
    time.toString + "_" + userId
  }
}