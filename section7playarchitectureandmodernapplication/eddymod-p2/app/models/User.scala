package models

import java.io.UnsupportedEncodingException
import java.security.{MessageDigest, NoSuchAlgorithmException}
import java.util.{Date, UUID}

import com.mongodb.casbah.Imports._
import com.novus.salat.dao.{ModelCompanion, SalatDAO}
import org.bson.types.ObjectId

case class User(
                 id: ObjectId = new ObjectId,
                 creationDate: Date = new Date,
                 email: String,
                 password: Array[Byte] = null,
                 firstName: String,
                 lastName: String,
                 authTokens: Map[String, Long] = Map[String,Long]()
                 )

object User extends UserDAO with UserUtil {

  import scala.concurrent.duration._

  private val tokenLifeTimeDays  = 7

  val TokenLifeTimeMillis = tokenLifeTimeDays.days.toMillis
}

trait UserDAO extends ModelCompanion[User, ObjectId] {
  def collection = Mongo()("users")

  val dao = new SalatDAO[User, ObjectId](collection) {}

  // Indexes
  collection.ensureIndex(DBObject("email" -> 1), "email", unique = true)

  // Queries
  def findOneByEmail(email: String): Option[User] = dao.findOne(MongoDBObject("email" -> email))

  def findOneByAuthToken(authToken: String): Option[User] = dao.findOne(
    DBObject(
      s"authTokens.$authToken" -> DBObject("$exists" -> true)
    )
  ) match {
    case Some(u) =>
      val filteredUser = u.copy(authTokens = u.authTokens.filter(_._2 + User.TokenLifeTimeMillis > System.currentTimeMillis()))
      User.save(
        filteredUser,
        WriteConcern.Safe
      )
      if (filteredUser.authTokens.exists(_._1 == authToken)) Some(filteredUser)
      else None
    case None => None
  }

  def authenticate(username: String, password: String): Option[User] = {
    findOne(
      DBObject("email" -> username, "password" -> User.getSha512(password))
    )
  }
}

trait UserUtil {

  def createToken(user: User): String = {
    val token = UUID.randomUUID.toString
    val authenticatedUser = user.copy(authTokens = user.authTokens + (token -> System.currentTimeMillis()))
    User.save(authenticatedUser, WriteConcern.Safe)
    token
  }

  def deleteAuthToken(user: User, token: String): User = user.copy(authTokens = user.authTokens.filterNot(_._1 == token))

  //Password encryption
  def getSha512(value: String): Array[Byte] = {
    try {
      MessageDigest.getInstance("SHA-512").digest(value.getBytes("UTF-8"))
    } catch {
      case e: NoSuchAlgorithmException => {
        throw new RuntimeException(e)
      }
      case e: UnsupportedEncodingException => {
        throw new RuntimeException(e)
      }
    }
  }
}


