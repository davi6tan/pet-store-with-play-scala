package util

import java.io.{File, FileOutputStream}
import java.util.UUID

import com.eduonix.common.util.ConfigLoader
import org.apache.commons.io.{FileUtils => ApacheFUtils}
import play.api.Play.current
import play.api.libs.ws.{WS, WSResponseHeaders}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scala.sys.process._
import scala.util.{Failure, Success, Try}

object FileUtils {
  val logger = Logger(this.getClass)
  lazy val tmpDir = ConfigLoader.conf.getString("tmp.dir")

  def newTmpSubdir = {
    val dir = new File(tmpDir, UUID.randomUUID().toString)
    dir.mkdirs()
    dir
  }

  def deleteDirectory(dir: File) = {
    ApacheFUtils.deleteDirectory(dir)
  }

  def getExtension(path: String): String = {
    if (path.toLowerCase().endsWith("tar.gz")) "tar.gz"
    else path.substring(path.lastIndexOf(".") + 1).toLowerCase
  }

  def newFile(dir: File) = {
    new File(dir, UUID.randomUUID().toString)
  }

  def getBaseName(filename: String) = {
    var lastIdx = filename.lastIndexOf(".")
    if (lastIdx == -1) lastIdx = filename.length
    filename.substring(0, lastIdx)
  }

  def isWfdbFile(filename: String) = {
    filename.endsWith(".dat") || filename.endsWith(".hea")
  }

  def extract(file: File, targetDir: File, extension: String) = {
    extension match {
      case "zip" => extractZip(file, targetDir)
      case "tar.gz" | "tgz" => extractTarGz(file, targetDir)
      case _ => throw new RuntimeException(s"unsupported extension $extension")
    }
    logger.info(s"exctracted $file with extension $extension")
  }
  def extractTarGz(file: File, targetDir: File) = {
    s"tar -xf $file -C $targetDir".!
  }

  def extractZip(file: File, targetDir: File) = {
    s"unzip $file -d $targetDir".!
  }

  def downloadUrl(url: String, dir: File = newTmpSubdir) = {
    import play.api.libs.iteratee._
    // Make the request
    val file = new File(dir, url.substring(url.lastIndexOf("/") + 1))
    Try {
      val futureResponse: Future[(WSResponseHeaders, Enumerator[Array[Byte]])] =
        WS.url(url).getStream()

      val futureFile: Future[File] = futureResponse.flatMap {
        case (headers, body) =>
          if(headers.status == 200) {
            val outputStream = new FileOutputStream(file)

            // The iteratee that writes to the output stream
            val iteratee = Iteratee.foreach[Array[Byte]] { bytes =>
              outputStream.write(bytes)
            }

            // Feed the body into the iteratee
            (body |>>> iteratee).andThen {
              case result =>
                // Close the output stream whether there was an error or not
                outputStream.close()
                // Get the result or rethrow the error
                result.get
            }.map(_ => file)
          } else {
            throw new RuntimeException(s"$url response status is ${headers.status}")
          }
      }
      futureFile
    } match {
      case Success(f) => f
      case Failure(t) => Promise.failed(t).future
    }

  }

}