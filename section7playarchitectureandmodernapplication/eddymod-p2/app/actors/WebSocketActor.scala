package actors

import akka.actor._
import akka.stream.ActorMaterializer
import play.api.libs.iteratee.Concurrent.Channel
import play.api.libs.json._
import models.JsonFormats._

import actors.WebSocketActor._


class WebSocketActor (out: ActorRef)  extends Actor {

  private implicit val materializer = ActorMaterializer()


  def receive = {
    case msg: EchoMessage  =>
      println(s"I received your message: $self   $EchoMessage ")

  }

}


object WebSocketActor {

  def props(out: ActorRef) = Props(new WebSocketActor(out))

  case class EchoMessage(msg: String)

}



object EchoWebSocketActor {
  def props(out: ActorRef) = Props(new EchoWebSocketActor(out))
}

class EchoWebSocketActor(out: ActorRef) extends Actor {
  def receive = {
    case msg: String =>
      println(s"actor, received message: $msg")
  }
}


