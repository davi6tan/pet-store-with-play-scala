# Play framework with Scala using activator


### Highlights

- Rest Api `play 2.5, play mailer`
- Unit testing `junit, scalacheck, scalatest, specs2, testng`
- Persistence with Slick and Reactive Mongo `slick3, Mysql, Reactive Mongo, Mongo DB`
- Implementing front end `angular, webjars`
- Reactive Web Application `AKKA , actors , reactive, AKKA Stream, AKKA Http`
- Security , Play with SSL and Web Services `CSRF, google mirror, OpenId, token based`
- Play architecture and moden application `lambda architecture, AKKA, Kafka, Cassandra, Zookeeper and Spark`       
         


Based on 

     Eduonix Learning Soultions Play Framework
